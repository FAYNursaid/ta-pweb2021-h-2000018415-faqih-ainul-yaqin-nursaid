<?php
// membuat array 2 dimensi yang berisi array asosiatif
$artikel = [
    [
        "judul" => "Menampilkan seluruh isi array dengan FOR dan FOREACH",
        "penulis" => "Faqih 'Ainul Yaqin Nursaid"
    ],
    [
        "judul" => "Mencetak struktur array",
        "penulis" => "Faqih 'Ainul Yaqin Nursaid"
    ],
    [
        "judul" => "PWeb2021-Tugas12: Server Side Programming | Array & Fungsi di PHP",
        "penulis" => "Faqih 'Ainul Yaqin Nursaid"
    ]
];

// menampilkan array
foreach($artikel as $post){
    echo "<h2>".$post["judul"]."</h2>";
    echo "<p>".$post["penulis"]."<p>";
    echo "<hr>";
}
?> 