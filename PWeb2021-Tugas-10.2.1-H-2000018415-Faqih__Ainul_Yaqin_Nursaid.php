<?php
@$nilai1 = $_POST['nilai1'];
?>

<!DOCTYPE html>
<html>
    <head>
        <title> Tugas 10.2.1 </title>
    </head>
    <body>
        <form method="POST">
            Masukan nilai : 
            <input type="text" name="nilai1" value="<?php echo $nilai1; ?>"/><br/>
            <input type="submit" name="submit" value="SUBMIT"/><br/><br/>
            <?php
            if ($nilai1 == "") {
                echo "";
            } else if ($nilai1 >= 0.00 && $nilai1 <= 39.99) {
                echo 'Nilai angka : ' . $nilai1 . ' ; Nilai huruf = E'.' ; Nilai numerik = 0.00';
            } else if ($nilai1 >= 40.00 && $nilai1 <= 43.74) {
                echo 'Nilai angka : ' . $nilai1 . ' ; Nilai huruf = D'.' ; Nilai numerik = 1.00';
            } else if ($nilai1 >= 43.75 && $nilai1 <= 51.24) {
                echo 'Nilai angka : ' . $nilai1 . ' ; Nilai huruf = D+'.' ; Nilai numerik = 1.33';
            } else if ($nilai1 >= 51.25 && $nilai1 <= 54.99) {
                echo 'Nilai angka : ' . $nilai1 . ' ; Nilai huruf = C-'.' ; Nilai numerik = 1.67';
            } else if ($nilai1 >= 55.00 && $nilai1 <= 57.49) {
                echo 'Nilai angka : ' . $nilai1 . ' ; Nilai huruf = C'.' ; Nilai numerik = 2.00';
            } else if ($nilai1 >= 57.50 && $nilai1 <= 62.49) {
                echo 'Nilai angka : ' . $nilai1 . ' ; Nilai huruf = C+'.' ; Nilai numerik = 2.33';
            } else if ($nilai1 >= 62.50 && $nilai1 <= 64.99) {
                echo 'Nilai angka : ' . $nilai1 . ' ; Nilai huruf = B-'.' ; Nilai numerik = 2.67';
            } else if ($nilai1 >= 65.00 && $nilai1 <= 68.74) {
                echo 'Nilai angka : ' . $nilai1 . ' ; Nilai huruf = B'.' ; Nilai numerik = 3.00';
            } else if ($nilai1 >= 68.75 && $nilai1 <= 76.24) {
                echo 'Nilai angka : ' . $nilai1 . ' ; Nilai huruf = B+'.' ; Nilai numerik = 3.33';
            } else if ($nilai1 >= 76.25 && $nilai1 <= 79.99) {
                echo 'Nilai angka : ' . $nilai1 . ' ; Nilai huruf = A-'.' ; Nilai numerik = 3.67';
            } else if ($nilai1 >= 80.00 && $nilai1 <= 100) {
                echo 'Nilai angka : ' . $nilai1 . ' ; Nilai huruf = A'.' ; Nilai numerik = 4.00';
            } else {
                echo 'Nilai : ' . $nilai1 . '; tidak bisa masuk penilaian.';
            }
            ?>
        </form>
    </body>
</html>