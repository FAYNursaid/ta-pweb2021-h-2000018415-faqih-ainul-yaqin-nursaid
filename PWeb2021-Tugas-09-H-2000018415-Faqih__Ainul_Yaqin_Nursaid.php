<h3>PWeb2021-Tugas09: Sintak PHP Dasar</h3>
<?php
$nim = "2000018415";
$nama = "Faqih 'Ainul Yaqin Nursaid";
$umur = "18";
$status = TRUE;

echo "Nama: $nama<br>";
echo "NIM: ".$nim."<br>";
print "Umur: ".$umur; print "<br>";
if ($status){
    echo "Status: aktif";
}else{
    echo "Status: tidak aktif";
}
echo "<br>";
echo "=====================================";
echo "<br>";
$a = 10;
$b = 5;
$c = $a + $b;
echo "Hasil dari Tambahan $a + $b = $c";
echo "</br>";
echo "=====================================";
echo "</br>";

$x = 10;
$y = 5;
$z = $x - $y;
echo "Hasil dari Pengurangan $x - $y = $z";
echo "</br>";
echo "=====================================";
echo "</br>";

$gaji =8650000;
$pajak = 0.2;
$thp = $gaji - ($gaji*$pajak);

echo "Gaji sebelum pajak = Rp.$gaji <br>";
echo "Gaji setelah dipotong dengan pajak = Rp. $thp <br>";
echo "=====================================";
echo "</br>";

$a = 7;
$b = 6;

echo "$a == $b : ". ($a == $b);
echo "<br> $a != $b : ". ($a != $b);
echo "<br> $a > $b : ". ($a > $b);
echo "<br> $a < $b : ". ($a < $b);
echo "<br> ($a == $b) && ($a > $b) : ". (($a != $b) && ($a > $b));
echo "<br> ($a == $b) || ($a > $b) : ". (($a != $b) || ($a > $b));
echo "</br>";
echo "=====================================";
?>